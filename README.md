# MAC0110-MiniEP10

MiniEP10 Coursework for MAC0110

This MiniEP10 coursework includes a single function, responsible for
receiving a NxN square matrix, and returning the array(s) that represent
the largest sum of elements you can get through a submatrix of the initial
given matrix. As suggested, if there is more than one possible result, the
function will return all of the submatrixes.
