include("miniep10.jl")
using Test

function testa_miniep10()
    print("Testando max_sum_submatrix...   ")
    @test max_sum_submatrix([1 1 1; 1 1 1; 1 1 1]) == [1 1 1; 1 1 1; 1 1 1]
    @test max_sum_submatrix([0 -1; -2 -3]) == [0]
    @test max_sum_submatrix([-1 1 1; 1 -1 1; 1 1 -2]) == [[1 1; -1 1],[1 -1; 1 1],[-1 1 1; 1 -1 1; 1 1 -2]]
    @test max_sum_submatrix([1 2 -1 4; -8 -3 4 2; 3 8 10 -8; -4 -1 1 7]) == [-3 4 2; 8 10 -8; -1 1 7]
    @test max_sum_submatrix([-5 -6 3 1 0; 9 7 8 3 7; -6 -2 -1 2 -4; -7 5 5 2 -6; 3 2 9 -5 1]) == [9 7 8 3; -6 -2 -1 2; -7 5 5 2; 3 2 9 -5]

    println("OK!")
    println(" ")
    printstyled("Todos os testes bem-sucedidos!", bold=true, color=:green)
end

testa_miniep10()