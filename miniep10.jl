function max_sum_submatrix(matrix)
    dim = size(matrix)
    if dim[1] != dim[2]
        return "A matriz dada não é quadrada!"
    end
    cache_maximos = []
    cache_matrizes = []
    for i in 1:dim[1]
        dimCalculada = ((dim[1]+1)-i)
        maxlocal = 0
        for j in 1:dimCalculada
            for k in 1:dimCalculada
                #percorrer as dimCalculada * dimCalculada possiveis para cada dimensao i
                submatriz = matrix[(j:(j+i-1)),(k:(k+i-1))]
                if somaElementos!(submatriz) >= maxlocal
                    maxlocal = somaElementos!(submatriz)
                    push!(cache_matrizes, submatriz)
                    push!(cache_maximos, maxlocal)
                end
            end
        end
    end
    res = convert(Array, resFinal!(cache_matrizes, cache_maximos))
    if length(res) == 1 # 2a CHECAGEM PARA DEBUGGING de Array{Any, 2}
        aux = []
        res = convert(Int, res[1])
        res = push!(aux, res)
    end
    return res
end
function somaElementos!(submatrix)
    soma = 0
    for i in 1:size(submatrix)[1]
        for j in 1:size(submatrix)[2]
            soma = soma + submatrix[i,j]
        end
    end
    return soma
end
function maior_do_vetor(vetor)
    maioratual = vetor[1]
    for i in 1:length(vetor)
        if vetor[i] > maioratual
            maioratual = vetor[i]
        else
            continue
        end
    end
    return maioratual
end
function resFinal!(vetormatrizes, vetormaximos)
    max = maior_do_vetor(vetormaximos)
    res = []
    for i in 1:length(vetormatrizes)
        if somaElementos!(vetormatrizes[i]) == max
            push!(res, vetormatrizes[i])
        end
    end
    if length(res) == 1 # 1a CHECAGEM PARA DEBUGGING de Array{Any, 2} 
        for i in 1:length(vetormatrizes)
            if somaElementos!(vetormatrizes[i]) == max
                return vetormatrizes[i]
            end
        end
    end
    return res
end